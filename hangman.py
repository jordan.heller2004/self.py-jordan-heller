import random

def print_hangman(num_of_tries):
    """ Prints the hangman drawing based on the number of unsuccesful tries
    :param num_of_tries: number of tries
"""
    HANGMAN_PHOTOS = {0 : "    x-------x",
                      1 :
"""    x-------x
    |
    |
    |
    |
    |
    """,
                      2 : 
"""    x-------x
    |       |
    |       0
    |
    |
    |
    """,
                      3 : 
"""    x-------x
    |       |
    |       0
    |       |
    |
    |
    """,
                      4 : 
"""    x-------x
    |       |
    |       0
    |      /|\\
    |
    |
""",
                      5 : 
"""    x-------x
    |       |
    |       0
    |      /|\\
    |      /
    |
""",
                      6 : 
"""    x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |
    """}
    print(HANGMAN_PHOTOS.get(num_of_tries))

def welcome_print():
    """Prints welcome page for the game hangman
"""

    print(""" Welcome to the game Hangman
  _    _                                         
 | |  | |                                        
 | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
 |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
 | |  | | (_| | | | | (_| | | | | | | (_| | | | |
 |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                      __/ |                      
                     |___/
""")


def check_win(secret_word, old_letters_guessed):
    """Checks if game has been won
    :param secret_word: Word we are trying to guess
    :param old_letters_guessed: All letters guessed until now
    :rtype: bool
"""
    letter_counter = 0
    for i in range(len(old_letters_guessed)):
        if(old_letters_guessed[i] in secret_word):
            letter_counter += 1
    return letter_counter == len(secret_word)


# In[5]:


def choose_word(file_path, index):
    """Choose word from path and index, returns number of words and word
    :param file_path: Path to file of words
    :param index: Index of word we will guess
    :rtype: tuple
"""
    word_dict = {}
    words = open(file_path).read().split(' ')
    
    for word in words:
        if word not in word_dict:
            word_dict[word] = 1
        
    return (len(word_dict), words[index % len(words) - 1])


# In[6]:


def show_hidden_word(secret_word, old_letters_guessed):
   """Prints the word you are trying to guess, only showing the correctly guessed letters
    :param secret_word: Word you need to guess
    :param old_letters_guessed: All letters guessed
    :rtype: str
"""
    show_str = ['_'] * len(secret_word)
    for i in range(len(secret_word)):
        if secret_word[i] in old_letters_guessed:
            show_str[i] = secret_word[i]
    return ' '.join(show_str)


# In[7]:


def is_valid_input(letter_guessed):
    """ Checks if letter guessed is valid
        :param letter_guessed: The user input
        :rtype: bool
    """
    return len(letter_guessed) == 1 and letter_guessed.isalpha()

def check_valid_input(letter_guessed, old_letters_guessed):
    """ Checks if input is valid and has not been guessed
        :param letter_guessed: The user input
        :param old_letters_guessed: All letters guessed
        :rtype: bool
    """
    return letter_guessed.lower() not in old_letters_guessed and is_valid_input(letter_guessed)

def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """ Check if input is valid, otherwise print X
        :param letter_guessed: The user input
        :param old_letters_guessed: All letters guessed
        :rtype: bool
    """
    return check_valid_input(letter_guessed, old_letters_guessed) or print("X\n"+" -> ".join(sorted(old_letters_guessed))) or False

def main():
    #print home screen
    welcome_print()
    
    #choose word
    word_info = choose_word(input("Enter file path: "), int(input("Enter index: ")))
    
    old_letters_guessed = []
    MAX_TRIES = 6
    num_of_tries = 0
    letter_guessed = '0'
        
    print_hangman(0)
    
    #run for one whole game
    while num_of_tries <= MAX_TRIES:
        valid = False
        
        #run until turn is valid
        while not valid:
            #get user guess
            letter_guessed = input("Guess a letter: ")
            #check if guess is valid, if it is add letter to list
            if try_update_letter_guessed(letter_guessed, old_letters_guessed):
                old_letters_guessed.append(letter_guessed)
                valid = True
        
        #show correctly guessed letters
        print(show_hidden_word(word_info[1], old_letters_guessed))
        
        #if guessed wrong, print hangman
        if letter_guessed not in word_info[1]:
            num_of_tries += 1
            print(":(")
            print_hangman(num_of_tries)
        
        # if game is over, break
        if check_win(word_info[1], old_letters_guessed):
            break
    
    # print WIN if won, LOSE if lost
    if check_win(word_info[1], old_letters_guessed):
        print("WIN")
    else:
        print("LOSE")

        
        
if __name__ == "__main__":
    main()


# In[ ]:




