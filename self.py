#1.3.2
import this

#1.4.1
import random
print(""" Welcome to the game Hangman
  _    _                                         
 | |  | |                                        
 | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
 |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
 | |  | | (_| | | | | (_| | | | | | | (_| | | | |
 |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                      __/ |                      
                     |___/
""", random.randint(5, 10))

#1.4.2
print("    x-------x")
print("""    x-------x
    |
    |
    |
    |
    |
""")
print("""    x-------x
    |       |
    |       0
    |
    |
    |
""")
print("""    x-------x
    |       |
    |       0
    |       |
    |
    |
""")

print("""    x-------x
    |       |
    |       0
    |      /|\
    |
    |
""")

print("""    x-------x
    |       |
    |       0
    |      /|\
    |      /
    |
""")

print("""    x-------x
    |       |
    |       0
    |      /|\
    |      / \
    |
""")

#2.3.3
num = input("Enter three digits")
sum = num % 10 + num // 100 + num // 10 % 10
print(sum)
print(sum // 3)
print(sum % 3)
print(not(sum % 3))

#2.5.1
HANGMAN_ASCII_ART = """ Welcome to the game Hangman
  _    _                                         
 | |  | |                                        
 | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
 |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
 | |  | | (_| | | | | (_| | | | | | | (_| | | | |
 |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                      __/ |                      
                     |___/
"""
MAX_TRIES = 6
print(HANGMAN_ASCII_ART, MAX_TRIES)

#2.5.2
guess = input("Guess a letter: ")
print(guess)

#3.2.1
print("\"Shuffle, Shuffle, Shuffle\", say it together!\nChange colors and directions,\nDon't back down and stop the player!\n\tDo you want to play Taki?\n\tPress y\\n")

#3.3.3
encrypted_message[::-2]

#3.4.2
message = input("Please enter a string: ")
print(message[0]+message[1::].replace(message[0], "e"))

#3.4.3
message = input("Please enter a string: ")
print(message[0:len(message)//2].lower() + message[len(message)//2:].upper())

#3.5.1
guess = input("Guess a letter: ")
print(guess.lower())

#3.5.2
word = input("Please enter a word: ")
print('_ '*len(word))

#4.2.2
word = input("Enter a word: ")
word = word.lower().replace(" ", "")
if word == word[::-1]:
    print("OK")
else:
    print("NOT")
    
#4.2.3
temperature = input("Insert the temperature you would like to convert: ")

if temperature[-1].lower() == 'c':
    celcius = (int)(temperature[:len(temperature)-1]) * 9 + 160
    celcius /= 5
    print((str)(celcius) + "F")
elif temperature[-1].lower() == 'f':
    fahrenheit = (int)(temperature[:len(temperature)-1]) * 5 - 160
    fahrenheit /= 9
    print((str)(fahrenheit) + "C")
    
#4.2.4
import calendar
date = input("Enter a date: ")
print(calendar.day_name[calendar.weekday((int)(date[6:]), (int)(date[3:5]) ,(int)(date[:2]))])

guess = input("Guess a letter: ")

#4.3.1
if(len(guess) > 1):
    if(not guess.isalpha()):
        print('E3')
    else:
        print('E1')
elif(not ('a' >= guess <='z')):
    print('E2')
else:
    print(guess.lower())

#5.3.4    
def last_early(my_str):
    return my_str.count(my_str[-1]) > 1
    
#5.3.5
def distance(num1, num2, num3):
    return (abs(num2 - num1) < 2) ^ (abs(num3 - num1) < 2)

#5.3.6
def fix_age(age):
    if(13 <= age <= 19 and not(age == 15 or age == 16)):
        age = 0
    return age

def filter_teens(a=13, b=13, c=13):
    return fix_age(a) + fix_age(b) + fix_age(c)
    
#5.3.7
def chocolate_maker(small, big, x):
    if(big > x // big):
        return (big - 1) * 5 + small >= x
    return big * 5 + small >= x

#5.4.1
def func(num1, num2):
    """This function returns the product of two numbers
    :param num1: first number
    :param num2: second number
    :return: The product of num1 and num2
    :rtype: int
    """
    return (int)(num1 * num2)

def main():
    print(func(2, 4))

if __name__ == "__main__":
    main()
    
#5.5.1
def is_valid_input(letter_guessed):
    return len(letter_guessed) == 1 and letter_guessed.isalpha()

#6.1.2
def shift_left(my_list):
    return my_list[1:] + my_list[:1]

#6.2.3
def format_list(my_list):
    return ', '.join(my_list[0:len(my_list):2]) + " and " + my_list[-1]

#6.2.4
def extend_list_x(list_x, list_y):
    list_x[len(list_x):] = list_y

#6.3.1
def are_lists_equal(list1, list2):
    return sorted(list1) == sorted(list2)

#6.3.2
def longest(my_list):
    return sorted(list1, key=len)[-1]

#6.4.1
def check_valid_input(letter_guessed, old_letters_guessed):
    return letter_guessed.lower() not in old_letters_guessed and is_valid_input(letter_guessed)

#6.4.2
def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    return check_valid_input(letter_guessed, old_letters_guessed) or print("X\n"+" -> ".join(sorted(old_letters_guessed))) or False

#7.1.4
def squared_numbers(start, stop):
    i = start
    squares = []
    while i <= stop:
        squares.append(i**2)
        i+=1
    return squares

#7.2.1
def is_greater(my_list, n):
    greater = []
    for num in my_list:
        if num > n :
            greater.append(num)
    return greater

#7.2.2
def numbers_letters_count(my_str):
    nums = 0
    for letter in my_str :
        if letter in '1234567890':
            nums += 1
    return [nums, len(my_str) - nums]

#7.2.4
def seven_boom(end_number):
    ret_list = []
    for i in range(end_number):
        if i % 7:
            ret_list.append(i)
        else:
            ret_list.append('BOOM')
    return ret_list

#7.2.5
def sequence_del(my_str):
    ret_str = my_str[0]
    for i in range(1, len(my_str)):
        if my_str[i] != my_str[i - 1]:
            ret_str += my_str[i]
    return ret_str
    
#7.2.6
def shopping_list(my_str):
    while True:
        option = int(input("Choose an option 1-9"))
        if option == 1:
            print(my_str)
            
        if option == 2:
            print(my_str.count(',') + 1)
        
        if option == 3:
            product = input("Enter a product name")
            print(product in my_str)
        
        if option == 4:
            product = input("Enter a product name")
            print(my_str.count(product))
        
        if option == 5:
            product = input("Enter a product name")
            index = my_str.index(product)
            my_str = my_str[:index - 1] + my_str[index + len(product):]
        
        if option == 6:
            product = input("Enter a product name")
            my_str += product
        
        if option == 7:
            prod_arr = my_str.split(',')
            for i in range(len(prod_arr)):
                if len(prod_arr[i]) < 3:
                    print(prod_arr[i])
        
        if option == 8:
            prod_arr = my_str.split(',')
            new_arr = []
            for i in range(len(prod_arr)):
                for j in range(i + 1, len(prod_arr)):
                    if i < j < len(prod_arr) and prod_arr[i] == prod_arr[j]:
                        prod_arr.remove(prod_arr[j])
            my_str = ','.join(prod_arr)
            
        if option == 9:
            break
#7.2.7
def arrow(my_char, max_length):
    for i in range(1, max_length * 2):
        print((max_length - abs(max_length - i)) * my_char)

#7.3.1
def show_hidden_word(secret_word, old_letters_guessed):
    show_str = ['_'] * len(secret_word)
    print(show_str)
    for i in range(len(secret_word)):
        if secret_word[i] in old_letters_guessed:
            show_str[i] = secret_word[i]
    return ' '.join(show_str)

#7.3.2
def check_win(secret_word, old_letters_guessed):
    letter_counter = 0
    for i in range(len(old_letters_guessed)):
        if(old_letters_guessed[i] in secret_word):
            letter_counter += 1
    return letter_counter == len(secret_word)
    
#8.2.1
data = ("self", "py", 1.543)
format_string = "Hello"

print("format_string %s.%s learner, you have only %.1f units left before you master the course!"% data)

#8.2.2
def sort_prices(list_of_tuples):
    def get_second_element(my_tuple):
        return float(my_tuple[1])
    
    return sorted(products, key=get_second_element, reverse=True)

#8.2.3
def mult_tuple(tuple1, tuple2):
    ret_list = []
    for i in range(len(tuple1)):
        for j in range(len(tuple2)):
            ret_list.append((tuple1[i], tuple2[j]))
            ret_list.append((tuple2[j], tuple1[i]))
    return tuple(ret_list)

#8.2.4
def sort_anagrams(list_of_strings):
    ret_list = []
    for i in range(len(list_of_strings)):
        current_list = [list_of_strings[i]]
        for j in range(i + 1, len(list_of_strings)):
            if sorted(list_of_strings[i]) == sorted(list_of_strings[j]):
                current_list.append(list_of_strings[j])
                list_of_strings[j] = ''
        if('' not in current_list):
            ret_list.append(current_list)
    return ret_list

#8.3.2
from datetime import date
my_dict = {"first_name" : "Mariah", "last_name" : "Carey", "birth_date" : "27.03.1970", "hobbies" : ["Sing","Compose", "Act"]}

user_input = int(input("Enter a number between 1-7:"))

if user_input == 1:
    print(my_dict["last_name"])
    
if user_input == 2:
    print(my_dict["birth_date"].split('.')[1])
    
if user_input == 3:
    print(my_dict["last_name"])
    
if user_input == 4:
    print(my_dict["hobbies"][-1])
    
if user_input == 5:
    my_dict["hobbies"].append("Cooking")
    
if user_input == 6:
    temp_list = my_dict["birth_date"].split('.')
    for i in range(len(temp_list)):
        temp_list[i] = int(temp_list[i])
    my_dict["birth_date"] = tuple(temp_list)
    print(my_dict["birth_date"])
    
if user_input == 7:
    if type(my_dict["birth_date"]) == tuple:
        my_dict["age"] = date.today().year - my_dict["birth_date"][2]
    elif type(my_dict["birth_date"]) == str:
            my_dict["age"] = date.today().year - int(my_dict["birth_date"].split('.')[2])
    print(my_dict["age"])

#8.3.3
def count_chars(my_str):
    ret_dict = {}
    for i in range(len(my_str)):
        if(my_str[i].isalpha()):
            if my_str[i] in ret_dict:
                ret_dict[my_str[i]] += 1
            else:
                ret_dict[my_str[i]] = 1
    return ret_dict

#8.3.4
def inverse_dict(my_dict):
    ret_dict = {}
    
    for value in my_dict.values():
        ret_dict[value] = []
        
    for item in my_dict.items():
        ret_dict[item[1]].append(item[0])
    
    return ret_dict

#8.4.1
def print_hangman(num_of_tries):
    HANGMAN_PHOTOS = {0 : "    x-------x",
                      1 :
"""    x-------x
    |
    |
    |
    |
    |
    """,
                      2 : 
"""    x-------x
    |       |
    |       0
    |
    |
    |
    """,
                      3 : 
"""    x-------x
    |       |
    |       0
    |       |
    |
    |
    """,
                      4 : 
"""    x-------x
    |       |
    |       0
    |      /|\\
    |
    |
""",
                      5 : 
"""    x-------x
    |       |
    |       0
    |      /|\\
    |      /
    |
""",
                      6 : 
"""    x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |
    """}
    print(HANGMAN_PHOTOS[num_of_tries])

#9.1.1
def are_files_equal(file1, file2):
    return open(file1).read() == open(file2).read()

#9.1.2
path = str(input("Enter a file path: "))
task = str(input("Enter a task: "))
contents = open(path).read()

if task == "sort":
    print(sorted(list(dict.fromkeys(contents.split(' ')))))

if task == "rev":
    rows = contents.split('\n')
    for row in rows:
        print(row[::-1])

if task == "last":
    n = int(input("Enter a number: "))
    rows = contents.split('\n')
    for i in reversed(range(n)):
        print(rows[len(rows) - i - 1])

#9.2.2
def copy_file_content(source, destination):
    copy = open(source).read()
    with open(destination, 'w') as destination_file:
        destination_file.write(copy)

#9.2.3
def who_is_missing(file_name):
    contents = open(file_name).read().split(',')
    for i in range(1, int(sorted(contents)[-1])):
        if str(i) not in contents:
            return i

#9.3.1
def my_mp3_playlist(file_path):
    def song_length(song):
        return song[2]
    ret_list = []
    content = open(file_path).read().split('\n')
    
    for i in range(len(content)):
        content[i] = content[i].split(';')[:3]
    
    song_dict = {}
    for i in range(len(content)):
        if content[i][1] in song_dict:
            song_dict[content[i][1]] += 1
        else:
            song_dict[content[i][1]] = 1
            
    ret_list.append(sorted(content, key=song_length)[-1][0])
    ret_list.append(len(content))
    ret_list.append(max(song_dict, key=song_dict.get))
    
    return tuple(ret_list)

#9.3.2
def my_mp4_playlist(file_path, new_song):
    songs = open(file_path).read().split('\n')
    for i in range(len(songs)):
        songs[i] = songs[i].split(';')[:3]
    replaced_row = ';'.join(songs[2]) + ';'
    songs[2][0] = new_song
    
    with open(file_path, "r+") as file:
        for i in range(len(songs)):
            file.write(';'.join(songs[i]) + ';\n')
    print("Printing the file: ")
    print(open(file_path).read())

#9.4.1
def choose_word(file_path, index):
    word_dict = {}
    words = open(file_path).read().split(' ')
    
    for word in words:
        if word not in word_dict:
            word_dict[word] = 1
        
    
    return (len(word_dict), words[index % len(words) - 1])